# Task32: Reactive Forms and Validations
- Convert the login and register forms in your previous task to use ReactiveForms
- Setup validation for each input
    - Required
    - Minimum Length