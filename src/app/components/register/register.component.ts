import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required,
                                    Validators.minLength(4),
                                    Validators.email ]),
    password: new FormControl('', [ Validators.required,
                                    Validators.minLength(4),
                                    Validators.maxLength(12)])
                
  });

  constructor(private authService: AuthService) { }

  get username() {
    return this.registerForm.get('username')
  }

  get password() {
    return this.registerForm.get('password')
  }


  ngOnInit(): void {
  }

  async onRegisterClick() {
    
    try {

      console.log(this.registerForm.value);
      

      const result: any = await this.authService.register(this.registerForm.value);

      console.log(result);
      
      
    } catch (e){
      console.error(e);
      
    }
    
  }

}
