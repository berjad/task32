import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required,
                                    Validators.minLength(4),
                                    Validators.email ]),
    password: new FormControl('', [ Validators.required,
                                    Validators.minLength(4),
                                    Validators.maxLength(12) ])
  });

  constructor(private authService: AuthService, private router: Router) { }

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }

  ngOnInit(): void {
  }

  async onLoginClick() {

    try {
      console.log(this.loginForm.value);
      
      const success: any = await this.authService.login(this.loginForm.value);
      console.log(success);
      
      if (success) {
        this.router.navigateByUrl('/dashboard');
      } else {
        alert('Wrong login!');
      }
    } catch (e) {
      console.error(e);
      
    }

    
  }

}
